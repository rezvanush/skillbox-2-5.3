<?php

error_reporting(E_ALL);
ini_set('display_errors',true);

require_once 'bootstrap.php';

$router = new \App\Router();

$router->get('/', function() {
    return new \App\View('indexs', ['title' =>'Главная страница сайта']);
});

// $router->get('/about/', \App\Controller::class . '@about');

$application = new \App\Application($router);
$application->run();