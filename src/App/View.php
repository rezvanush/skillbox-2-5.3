<?php
namespace App;

class View implements Renderable
{
	public $page;
	public $title;

	public function __construct(string $page, array $meta = [])
	{
		$this->page = VIEW_DIR . str_replace('.', '/', $page) . '.php';
		$this->title = isset($meta['title']) ? $meta['title'] : '';
	}

	public function render()
	{
		include VIEW_DIR . '/header.php';

		if (!file_exists($this->page)) {
			throw new \App\Exception\NotFoundException("Not Found");
		}

		include $this->page;
		
		include VIEW_DIR . '/footer.php';
	}
}