<?php
namespace App;

class Application
{
	public $router;

	public function __construct(Router $router)
	{
		$this->router = $router;
	}

	public function run()
	{
		try {
			$view = $this->router->dispatch();

			if ($view instanceof Renderable) {
				return $view->render();
			}

			echo $view;
		} catch (\Exception $e) {
			$this->renderException($e);
		}
	}

	public function renderException(\Exception $e)
	{
		if ($e instanceof Renderable) {
			return $e->render();
		}

		$eCode = ($e->getCode() === 0) ? 500 : $e->getCode();

		echo $eCode . ': ' . $e->getMessage();
	}
}
