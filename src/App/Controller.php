<?php
namespace App;

class Controller
{
	public static function index()
	{
		return "home";
	}

	public static function about()
	{
		return "about";
	}

	public static function __callStatic($name, $arguments)
	{
		return "Метод $name не определен";
	}
}