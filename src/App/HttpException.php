<?php
namespace App;

class HttpException extends \Exception {}

class NotFoundException extends HttpException implements \App\Renderable
{
	public function render()
	{
		echo '404 Not Found';
	}
}