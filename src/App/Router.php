<?php
namespace App;

class Router
{
	public $url;
	public $callback;

	public function get($url = false, $callback = false)
	{
		$this->url = $url;
		$this->callback = $callback;
	}

	public function dispatch()
	{
		if ($_SERVER['REQUEST_URI'] == $this->url) {

			if (is_callable($this->callback)) {
				$callback = $this->callback;
				return $callback();
			}

			$posSeparator = stripos($this->callback, '@');
			$callbackClass = substr($this->callback, 0, $posSeparator);
			$callbackMethod = substr($this->callback, $posSeparator + 1);
			
			$controller = new $callbackClass;
			return $controller->$callbackMethod();
		}
	}

}